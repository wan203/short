@extends('layouts.app')
@section('content')
    <h1 class="mt-3">List</h1>
    @if(count($shortens)>0)
        @foreach($shortens as $shorten)
            <div>
                <p>{{$shorten->created_at}}</p>
                <a href="{{url($shorten->long_url)}}">
                    <p> class="text-warning">{{$shorten->long_url}}</p>
                </a>
                <input id="shorturl{{$shorten->id}}" class="form-control" type="text"
                       value="http://www.short.local/t/{{$shorten->short_url}}" readonly>
                <button onclick="copy(this)" value="{{$shorten->id}}" type="button"
                        class="btn btn-dark" id="copyBtn">copy</button>
                <p>{{$shorten->view}}</p>
            </div>
            <hr>
        @endforeach
    @endif

    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl' + id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied' + copyText.value);
        }
    </script>

@endsection

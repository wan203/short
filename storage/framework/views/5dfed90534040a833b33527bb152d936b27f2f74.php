<?php $__env->startSection('content'); ?>

    <h2 style="text-align: center">Create Short URL</h2>
    <form method="post" action="<?php echo e(url('/')); ?>" >
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <input type="text" name ="long" placeholder="Input long URL" class="form-control" >
        </div>
        <button type="submit" class="btn btn-dark">CREATE SHORT URL</button>
    </form>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\short\resources\views/new.blade.php ENDPATH**/ ?>
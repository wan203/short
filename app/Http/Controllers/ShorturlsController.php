<?php

namespace App\Http\Controllers;

use App\Shorturl;
use Illuminate\Http\Request;

class ShorturlsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shortens = Shorturl::OrderBy('created_at','desc')->get();
        return view('index')->with('shortens',$shortens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $charset = "abcdefghijklmnopqrstuvwxyz";
        $numset = "0123456789";
        $url = "";

        for($i=0; $i<5; $i++)
        {
            $url = $url.$numset[rand(0,9)];
        }
        $url .=$charset[rand(0,strlen($charset))-1];

        $this->validate($request,
            [
                'long' => 'required',
            ]
        );
        $shorten = new Shorturl();
        $shorten->long_url = $request->input('long');
        $shorten->short_url = $url;
        $shorten->view=0;
        $shorten->save();

        return redirect('/new')->with('success','Success'.' http://short.local/'.$url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorturl)
    {
        //
        $shortens = Shorturl::all();
        if(count($shortens)>0){
            foreach($shortens as $shorten){
                if($shorten->short_url == $shorturl){
                    $shorten->view +=1;
                    $shorten->save();
                    return view('redirect')->with('longurl',$shorten->long_url);
                }
            }
        }
        return view('notfound');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

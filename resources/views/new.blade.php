@extends('layouts.app')
@section('content')

    <h2 style="text-align: center">Create Short URL</h2>
    <form method="post" action="{{url('/')}}" >
        @csrf
        <div class="form-group">
            <input type="text" name ="long" placeholder="Input long URL" class="form-control" >
        </div>
        <button type="submit" class="btn btn-dark">CREATE SHORT URL</button>
    </form>
@endsection


